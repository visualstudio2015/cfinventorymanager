﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InventoryManager2
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        MainWindow myMainWindow = null;

        public Login(MainWindow aMainWindow)
        {
            InitializeComponent();
            myMainWindow = aMainWindow;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            myMainWindow.IsEnabled = true;
            DialogResult = true;
           // myMainWindow.Show();
        }
    }
}
