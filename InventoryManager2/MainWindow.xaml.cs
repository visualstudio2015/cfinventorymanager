﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InventoryManager2.Views;
using InventoryManager2.Classes;

namespace InventoryManager2
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            
            Title = consts.appName;
            presenterMain.Content = null;
            addMainControl(new Views.UCMaterialGrid());
        }

        private void addMainControl(UCMaterialGrid uCMaterialGrid)
        {
            presenterMain.Content = uCMaterialGrid;
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void CommandBinding_Executed_1(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void CommandBinding_Executed_2(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            CleanUp();
            Close();
        }

        private void CleanUp()
        {
            // clean up all database connections and so on
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = false;
         
          
            PromptCredentialsResult cred = null;
            PromptCredentialsSecureStringResult credSecure = null;
            var defaultUserName = "kroenig".ToSecureString();
            credSecure = CredentialUI.PromptWithSecureString("Login to this application", "Login to access.");

            /* Credentialmanager
            CredentialUI.PromptForCredentialsFlag flag = CredentialUI.PromptForCredentialsFlag.CREDUI_FLAGS_VALIDATE_USERNAME;
            NetworkCredential cred = CredentialManager.PromptForCredentials(machineName, ref doSave, "Login to access this host.","Login");
            */
            if (credSecure == null)
                Close();
            else
            {
                string UserName = credSecure.UserName.ToInsecureString();
                string Password = credSecure.Password.ToInsecureString();
                if (!checkInDataBase(UserName, Password))
                {
                    Close();
                    return;
                } else
                {
                    this.IsEnabled = true;
                }
            }
        }

        private bool checkInDataBase(string userName, string password)
        {
            // dummy result
            return true;
        }
    }
}
