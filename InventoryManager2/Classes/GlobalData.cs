﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;

namespace InventoryManager2.Classes
{
    public class GlobalData: IDisposable
    {
        public SQLiteConnection con = null;
        public SQLiteCommand com = null;
        public SQLiteDataAdapter sa = null;
        public string lastErrorString = "";

        public GlobalData(string dataFileName)
        {
            con = new SQLiteConnection(string.Format("Data Source={0};Version=3;datetimeformat=CurrentCulture", dataFileName));
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                lastErrorString = ex.Message;
                // and what is that now?????
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // Dient zur Erkennung redundanter Aufrufe.

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (con.State == System.Data.ConnectionState.Open)
                        con.Close();
                }
                sa = null;
                con = null;
                disposedValue = true;
            }
        }

        // TODO: Finalizer nur überschreiben, wenn Dispose(bool disposing) weiter oben Code für die Freigabe nicht verwalteter Ressourcen enthält.
        // ~GlobalData() {
        //   // Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(bool disposing) weiter oben ein.
        //   Dispose(false);
        // }

        // Dieser Code wird hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        public void Dispose()
        {
            // Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(bool disposing) weiter oben ein.
            Dispose(true);
            // TODO: Auskommentierung der folgenden Zeile aufheben, wenn der Finalizer weiter oben überschrieben wird.
            // GC.SuppressFinalize(this);
        }
        #endregion

        #region data retrieve
        public System.Data.DataTable getTable(string sql, System.Collections.Hashtable ht)
        {
            lastErrorString = "";
            SQLiteCommand command = new SQLiteCommand(sql, con);
            if (ht != null)
            {
                foreach (object key in ht.Keys)
                {
                    string akey = (string)key;
                    object value = ht[key];
                    command.Parameters.AddWithValue(akey, value);
                }
            }
            SQLiteDataAdapter sa = new SQLiteDataAdapter(command);
            DataSet ds = new DataSet();
            try
            {
                sa.Fill(ds);
            }
            catch (Exception ex)
            {
                lastErrorString = ex.Message;
                return null;
            }
            DataTable dt = ds.Tables[0];
            ds = null;
            sa.Dispose();
            sa = null;
            return dt;
        }
        #endregion

        #region data execute
        public int execute(string sql, System.Collections.Hashtable ht)
        {
            lastErrorString = "";
            SQLiteCommand command = new SQLiteCommand(sql, con);
            if (ht != null)
            {
                foreach (object key in ht.Keys)
                {
                    string akey = (string)key;
                    object value = ht[key];
                    command.Parameters.AddWithValue(akey, value);
                }
            }
            int ret = 0;
            try
            {
                ret = command.ExecuteNonQuery(CommandBehavior.Default);

            }
            catch (Exception ex)
            {
                lastErrorString = ex.Message;
            }
            return ret;
        }
        #endregion
    }
}
