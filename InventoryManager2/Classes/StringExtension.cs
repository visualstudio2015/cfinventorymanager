﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace InventoryManager2.Classes
{
   public static class StringExtension
   { 
        public static SecureString ToSecureString(this String s)
        {
            SecureString ss = new SecureString();
            foreach (var c in s)
                ss.AppendChar(c);
            ss.MakeReadOnly();
            return ss;
        }

        public static String ToInsecureString(this SecureString s)
        {
            IntPtr p = IntPtr.Zero;
            try
            {
                p = Marshal.SecureStringToCoTaskMemUnicode(s);
                return Marshal.PtrToStringUni(p);
            }
            finally
            {
                if (p != IntPtr.Zero)
                    Marshal.ZeroFreeCoTaskMemUnicode(p);
            }
        }
        public static SecureString PtrToSecureString(IntPtr p)
        {
            SecureString s = new SecureString();
            Int32 i = 0;
            while (true)
            {
                Char c = (Char)Marshal.ReadInt16(p, ((i++) * sizeof(Int16)));
                if (c == '\u0000')
                    break;
                s.AppendChar(c);
            }
            s.MakeReadOnly();
            return s;
        }
        public static SecureString PtrToSecureString(IntPtr p, Int32 length)
        {
            SecureString s = new SecureString();
            for (var i = 0; i < length; i++)
                s.AppendChar((Char)Marshal.ReadInt16(p, i * sizeof(Int16)));
            s.MakeReadOnly();
            return s;
        }
    }
}
