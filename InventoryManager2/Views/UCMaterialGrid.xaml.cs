﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace InventoryManager2.Views
{
    /// <summary>
    /// Interaktionslogik für UCMaterialGrid.xaml
    /// </summary>
    public partial class UCMaterialGrid : UserControl
    {
        public UCMaterialGrid()
        {
            InitializeComponent();
            getAllMaterial();
        }

        private void getAllMaterial()
        {
            DataTable dtMaterial = consts.gdata.getTable("select * from material",null);
            if (dtMaterial == null)
                return;
            this.DataContext = dtMaterial.DefaultView;
           // this.Datagrid1.ItemsSource = dtMaterial.DefaultView;

        }
    }
}
