USE [master]
GO
/****** Object:  Database [CodefrogInventory]    Script Date: 17.01.2018 20:53:11 ******/
CREATE DATABASE [CodefrogInventory]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CodefrogInventory', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\CodefrogInventory.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CodefrogInventory_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\CodefrogInventory_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CodefrogInventory] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CodefrogInventory].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CodefrogInventory] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CodefrogInventory] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CodefrogInventory] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CodefrogInventory] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CodefrogInventory] SET ARITHABORT OFF 
GO
ALTER DATABASE [CodefrogInventory] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CodefrogInventory] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CodefrogInventory] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CodefrogInventory] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CodefrogInventory] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CodefrogInventory] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CodefrogInventory] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CodefrogInventory] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CodefrogInventory] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CodefrogInventory] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CodefrogInventory] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CodefrogInventory] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CodefrogInventory] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CodefrogInventory] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CodefrogInventory] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CodefrogInventory] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CodefrogInventory] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CodefrogInventory] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CodefrogInventory] SET  MULTI_USER 
GO
ALTER DATABASE [CodefrogInventory] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CodefrogInventory] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CodefrogInventory] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CodefrogInventory] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CodefrogInventory] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CodefrogInventory] SET QUERY_STORE = OFF
GO
USE [CodefrogInventory]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [CodefrogInventory]
GO
/****** Object:  Table [dbo].[Adresses]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adresses](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Postcode] [nvarchar](50) NULL,
	[City] [nvarchar](100) NULL,
	[Streetadress1] [nvarchar](256) NULL,
	[StreetAdress2] [nvarchar](256) NULL,
	[Country] [nvarchar](128) NOT NULL,
	[Language] [nvarchar](50) NULL,
	[Skills] [nvarchar](50) NULL,
 CONSTRAINT [PK_Adresses] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AdressLinkTable]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdressLinkTable](
	[EmployeeId] [int] NOT NULL,
	[AdressID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](128) NULL,
	[AddressLink] [int] NULL,
	[PhoneLink] [int] NULL,
	[DateIn] [date] NOT NULL,
	[DateOut] [date] NULL,
	[Status] [nchar](10) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeeLinkToLendMaterial]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeLinkToLendMaterial](
	[EmployeeId] [int] NOT NULL,
	[MaterialLendID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Material]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Material](
	[id] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](512) NULL,
	[MaterialGroup] [int] NOT NULL,
	[SerialNumber] [nvarchar](50) NULL,
	[InternalInventoryNumber] [nvarchar](50) NULL,
	[AmountOnStock] [int] NOT NULL,
	[Supplier] [nvarchar](128) NULL,
	[PurchasePrice] [float] NULL,
 CONSTRAINT [PK_Material] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaterialGroups]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialGroups](
	[id] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](256) NULL,
 CONSTRAINT [PK_MaterialGroups] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaterialLendToEmployee]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialLendToEmployee](
	[id] [int] NOT NULL,
	[MaterialID] [int] NOT NULL,
	[GivenDate] [datetime] NOT NULL,
	[GiveBackDate] [datetime] NULL,
	[Amount] [int] NULL,
 CONSTRAINT [PK_MaterialLendToEmployee] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[PasswordHashed] [nvarchar](128) NOT NULL,
	[Role] [int] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRoleLink]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoleLink](
	[UserId] [int] NOT NULL,
	[RoleID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 17.01.2018 20:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_AdressLinkTable]    Script Date: 17.01.2018 20:53:11 ******/
CREATE NONCLUSTERED INDEX [IX_AdressLinkTable] ON [dbo].[AdressLinkTable]
(
	[EmployeeId] ASC,
	[AdressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Employee]    Script Date: 17.01.2018 20:53:11 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Employee] ON [dbo].[Employee]
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EmployeeLinkToLendMaterial]    Script Date: 17.01.2018 20:53:11 ******/
CREATE NONCLUSTERED INDEX [IX_EmployeeLinkToLendMaterial] ON [dbo].[EmployeeLinkToLendMaterial]
(
	[EmployeeId] ASC,
	[MaterialLendID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Material]    Script Date: 17.01.2018 20:53:11 ******/
CREATE NONCLUSTERED INDEX [IX_Material] ON [dbo].[Material]
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Material_1]    Script Date: 17.01.2018 20:53:11 ******/
CREATE NONCLUSTERED INDEX [IX_Material_1] ON [dbo].[Material]
(
	[MaterialGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MaterialLendToEmployee]    Script Date: 17.01.2018 20:53:11 ******/
CREATE NONCLUSTERED INDEX [IX_MaterialLendToEmployee] ON [dbo].[MaterialLendToEmployee]
(
	[MaterialID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_User]    Script Date: 17.01.2018 20:53:11 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_User] ON [dbo].[User]
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserRoleLink]    Script Date: 17.01.2018 20:53:11 ******/
CREATE NONCLUSTERED INDEX [IX_UserRoleLink] ON [dbo].[UserRoleLink]
(
	[UserId] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AdressLinkTable]  WITH CHECK ADD  CONSTRAINT [FK_AdressLinkTable_Adresses] FOREIGN KEY([AdressID])
REFERENCES [dbo].[Adresses] ([id])
GO
ALTER TABLE [dbo].[AdressLinkTable] CHECK CONSTRAINT [FK_AdressLinkTable_Adresses]
GO
ALTER TABLE [dbo].[AdressLinkTable]  WITH CHECK ADD  CONSTRAINT [FK_AdressLinkTable_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[AdressLinkTable] CHECK CONSTRAINT [FK_AdressLinkTable_Employee]
GO
ALTER TABLE [dbo].[EmployeeLinkToLendMaterial]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeLinkToLendMaterial_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([id])
GO
ALTER TABLE [dbo].[EmployeeLinkToLendMaterial] CHECK CONSTRAINT [FK_EmployeeLinkToLendMaterial_Employee]
GO
ALTER TABLE [dbo].[EmployeeLinkToLendMaterial]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeLinkToLendMaterial_MaterialLendToEmployee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[MaterialLendToEmployee] ([id])
GO
ALTER TABLE [dbo].[EmployeeLinkToLendMaterial] CHECK CONSTRAINT [FK_EmployeeLinkToLendMaterial_MaterialLendToEmployee]
GO
ALTER TABLE [dbo].[Material]  WITH CHECK ADD  CONSTRAINT [FK_Material_MaterialGroups] FOREIGN KEY([MaterialGroup])
REFERENCES [dbo].[MaterialGroups] ([id])
GO
ALTER TABLE [dbo].[Material] CHECK CONSTRAINT [FK_Material_MaterialGroups]
GO
ALTER TABLE [dbo].[MaterialLendToEmployee]  WITH CHECK ADD  CONSTRAINT [FK_MaterialLendToEmployee_Material] FOREIGN KEY([MaterialID])
REFERENCES [dbo].[Material] ([id])
GO
ALTER TABLE [dbo].[MaterialLendToEmployee] CHECK CONSTRAINT [FK_MaterialLendToEmployee_Material]
GO
ALTER TABLE [dbo].[UserRoleLink]  WITH CHECK ADD  CONSTRAINT [FK_UserRoleLink_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([id])
GO
ALTER TABLE [dbo].[UserRoleLink] CHECK CONSTRAINT [FK_UserRoleLink_User]
GO
ALTER TABLE [dbo].[UserRoleLink]  WITH CHECK ADD  CONSTRAINT [FK_UserRoleLink_UserRoles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[UserRoles] ([id])
GO
ALTER TABLE [dbo].[UserRoleLink] CHECK CONSTRAINT [FK_UserRoleLink_UserRoles]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'in Role define "material:read" or "material:edit" or "employee:read" or "employee:edit"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRoles'
GO
USE [master]
GO
ALTER DATABASE [CodefrogInventory] SET  READ_WRITE 
GO
