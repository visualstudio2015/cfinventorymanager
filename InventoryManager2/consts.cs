﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace InventoryManager2
{
    public static class consts
    {
        public static int ImageDelay = 5;
        public static bool forceDebug = false;
        public static string appName = "Codefrog InventoryManager";
        public static string appPath = AppDomain.CurrentDomain.BaseDirectory;
        public static string dataBasePath = "data";
        public static string dataBaseName = "";
        public static InventoryManager2.Classes.GlobalData gdata = null;

        public static void init()
        {
            string mypath = Path.Combine(appPath, dataBasePath);
            dataBaseName = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\Codefrog InventoryManager";
            dataBaseName = mypath;
            if (!System.IO.Directory.Exists(dataBaseName))
                System.IO.Directory.CreateDirectory(dataBaseName);
            dataBaseName += Path.Combine(dataBaseName, @"\CodefrogInventoryManager.db");
            gdata = new Classes.GlobalData(dataBaseName);
        }
    }

}
